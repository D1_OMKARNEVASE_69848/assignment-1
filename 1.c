#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{

int max;
if(argc == 1 )
{
    printf("Please input the number of integers preceding with ./a.exe number\n");
    return 1;
}

    max = atoi(argv[0]);

    for ( int c = 1 ; c < argc ; c++ )
    {
        if ( atoi(argv[c]) > max )
        {
           max = atoi(argv[c]);

        }
    }
 printf("Maximum element is : %d\n",max);
}